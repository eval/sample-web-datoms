#!/usr/bin/env ohmyclj
"DEPS='net.sourceforge.htmlcleaner/htmlcleaner=2.6;clj-http=0.7.9;com.github.kyleburton/clj-xpath=1.4.3'"

(ns api)

(defmulti profile (fn [& args] (first args)))

(ns user
  (:import
   [org.htmlcleaner HtmlCleaner DomSerializer CleanerProperties])
  (:require
   [api :as api]
   [clj-http.client :as client]
   [clojure.string :as str])
  (:use
   [clj-xpath.core :only [$x:node* $x:text*]]))


(defmethod api/profile :github-stats [_ {:keys [repository]}]
  {:url         (str "https://github.com/" repository)
   #_#_:data-ns (str/join "." (apply conj ["gh-stats"] (str/split repository #"/")))
   :data        #:gh-stats {:title {:xp      "//span[@itemprop='about']"
                                    :process #(-> % first (str/trim))}
                            :stars {:xp      (str "//a[@href='/" repository "/stargazers']")
                                    :process #(-> % first (str/replace "," "") (read-string))}}})


(defmethod api/profile :reddit [_ {:keys [sub]}]
  {:url         (str "https://old.reddit.com/r/" sub)
   #_#_:data-ns (str/join "." (apply conj ["gh-stats"] (str/split repository #"/")))
   :data        (into
                 {:subscribers {:xp      "//span[@class='subscribers']/span[1]"
                                :process #(-> % first (str/replace "," "") (read-string))}}
                 (for [n    (range 1 4)
                       s    {"title" [(str "//div[@id='siteTable']//span[text()="
                                           n "]/following-sibling::div[@class='entry unvoted']//p/a")
                                      #(-> % first)]
                             "url"   [(str "//div[@id='siteTable']//span[text()="
                                           n "]/following-sibling::div[@class='entry unvoted']//p/a/@href")
                                      #(-> % first)]
                             "score" [(str "//div[@id='siteTable']//span[text()="
                                           n "]/following-sibling::div[1]/div[@class='score likes']")
                                      #(-> % first (read-string))]}
                       :let [[prop [xp process]] s]]
                   [(keyword (str "top." n) prop)
                    {:xp xp :process process}]))})


(defn html->xml
  [a-html-doc]
  (let [cleaner       (new HtmlCleaner)
        cleaner-props (new CleanerProperties)
        dom-srlzr     (new DomSerializer cleaner-props)
        cleaned-doc   (.clean cleaner a-html-doc)]
    (.createDOM dom-srlzr cleaned-doc)))


(defn url->xml [url]
  (let [headers {"User-Agent" "Mozilla/5.0 (Windows NT 6.1;) Gecko/20100101 Firefox/13.0.1"}]
    (-> url (client/get {:headers headers}) :body (html->xml))))



(defn fetch-data [url data]
  (let [xml (url->xml url)]
    (reduce (fn [acc [k {:keys [xp process] :or {process #(-> % first (str/trim))}}]]
              (assoc acc k (->> xml ($x:text* xp) process)))
            {}
            data)))


(defn fetch-profile [profile-name profile-args]
  (let [{:keys [url data]} (api/profile profile-name profile-args)]
    (fetch-data url data)))


(defn -main [& args]
  (clojure.pprint/pprint (fetch-profile :reddit {:sub "Clojure"})))


(ns test
  (:require
   [clojure.test :refer [deftest is testing are]]
   [user]))

(deftest main-test
  (is (= '(1 2) (user/-main 1 2)) "FIXME"))
